import sys, time
import praw, prawcore
import psycopg2
import configparser
from apscheduler.schedulers.background import BackgroundScheduler
from datetime import datetime
from collections import deque

from layer7_utilities import oAuth, Logger
from Const import FEATURES

__botname__     = '2TAAB'
__description__ = '2 Tokens And A Blue - r/DTG Currency & April Fools Bot'
__author__      = 'u/D0cR3d'
__version__     = '1.1.3'
__subname__     = 'DestinyTheGame'

LEADERBOARD_WIKI = 'misc/leaderboard'

class TokenBot():
	def __init__(self):
		self.logger = Logger(__botname__, __version__)
		self.logger.info('//********** Started {} Version {} **********\\\\'.format(__botname__, __version__))
		self.canRun = False

		self.login()

		if self.canRun:
			self.me = self.r.user.me()
			self.subreddit = self.r.subreddit(__subname__)
			self.__init_database()
			self.done = deque(maxlen=30000)
			self.gilded_done = deque(maxlen=5000)
			self.getProcessed()

			# Set the hourly task
			self.scheduler = BackgroundScheduler()
			self.scheduler.add_job(self.update_leaderboard, 'interval', hours=1)
			self.scheduler.start()
			self.logger.info('Scheduled leaderboard task')

			for job in self.scheduler.get_jobs():
				job.func()
				self.logger.info('Triggered initial leaderboard update')


	def __init_configs(self):
		config = configparser.ConfigParser()
		config.read("/opt/skynet/Configs/config.ini")
		self.DatabaseName = '2TAAB'
		self.DB_USERNAME = config.get('Database', 'Username')
		self.DB_PASSWORD = config.get('Database', 'Password')
		self.DB_HOST = config.get('Database', 'Host')

	def __init_database(self):
		self.__init_configs()
		self.con = psycopg2.connect(host=self.DB_HOST, dbname=self.DatabaseName, user=self.DB_USERNAME, password=self.DB_PASSWORD)
		self.con.autocommit = True
		self.cur = self.con.cursor()
		self.logger.debug('Connected to database')

	def __str__(self):
		return self.me.name

	def login(self):
		try:
			auth = oAuth("2TAAB", __description__, __version__, __author__, __botname__)
			for account in auth.accounts:
				self.r = account.login()
			self.logger.info('Connected to account: {}'.format(self.r.user.me()))
			self.canRun = True
		except:
			self.logger.error('Failed to log in.')
			self.canRun = False

	def getProcessed(self):
		self.cur.execute("SELECT thing_fullname FROM token_log ORDER BY timestamp_utc DESC LIMIT 20000")
		fetched = self.cur.fetchall()

		# Gets the most recent gilded items separately as they don't change often but could get lost in the log
		self.cur.execute("SELECT thing_fullname FROM token_log WHERE feature_code='is_gilded' ORDER BY timestamp_utc DESC LIMIT 600")
		gildedfetched = self.cur.fetchall()
		
		for item in fetched:
			self.done.append(item[0])

		for gildedfetched in fetched:
			self.gilded_done.append(item[0])

		self.logger.debug('Got all processed things')


	def checkInbox(self):
		for message in self.r.inbox.unread(limit=None):
			self.logger.info('{} | Processing Unread Inbox, MailID: {}'.format(self.me, message.name))
			#message.mark_read()

			if message.body.startswith('**gadzooks!'):
				message.subreddit.mod.accept_invite()
				message.mark_read()
				continue
			else:
				message.mark_read()

	def action(self):
		self.checkInbox()
		self.checkContent()

	def checkContent(self):
		toProcess = []
		toProcess_gilded = []

		self.logger.debug('{} | Getting Reddit New'.format(self.me))
		for post in self.subreddit.new(limit=200):
			if not post.fullname in self.done:
				self.logger.debug("{} | Added Post to toProcess - {}".format(self.me, post.fullname))
				self.done.append(post.fullname)
				toProcess.append(post)

		self.logger.debug('{} | Getting Reddit Comments'.format(self.me))
		for comment in self.subreddit.comments(limit=300):
			if not comment.fullname in self.done:
				self.logger.debug("{} | Added comment to toProcess - {}".format(self.me, comment.fullname))
				self.done.append(comment.fullname)
				toProcess.append(comment)

		self.logger.debug('{} | Getting Reddit Spam'.format(self.me))
		for spam in self.subreddit.mod.spam(limit=200):
			if not spam.fullname in self.done:
				self.logger.debug("{} | Added spam to toProcess - {}".format(self.me, spam.fullname))
				self.done.append(spam.fullname)
				toProcess.append(spam)

		self.logger.debug('{} | Getting Reddit Gilds'.format(self.me))
		for gild in self.subreddit.gilded(limit=2):
			if not gild.fullname in self.gilded_done:
				self.logger.debug("{} | Added gilded to toProcess - {}".format(self.me, gild.fullname))
				self.gilded_done.append(gild.fullname)
				toProcess_gilded.append(gild)

		for thing in toProcess:
			self.processItem(thing, False)

		for thing in toProcess_gilded:
			self.processItem(thing, True)

	def processItem(self, thing, is_gilded):
		try:
			multiplier = None
			buff_or_debuff_type = None

			if self.checkNameStartsWith(thing.author.name, 'n'):
				ftr_name = 'username_starts_with_n_buff'
				multiplier = FEATURES[ftr_name]['ftr_value']
				buff_or_debuff_type = FEATURES[ftr_name]['value_type']

			if self.checkNameStartsWith(thing.author.name, 'f'):
				ftr_name = 'username_starts_with_f_debuff'
				multiplier = FEATURES[ftr_name]['ftr_value']
				buff_or_debuff_type = FEATURES[ftr_name]['value_type']

			if is_gilded:
				if thing.gilded > 0:
					ftr_name = 'is_gilded'
					self.adjustTokens(thing, FEATURES[ftr_name]['value_type'], FEATURES[ftr_name]['ftr_value'], ftr_name, multiplier, buff_or_debuff_type)
					return

			if isinstance(thing, praw.models.Comment):
				# They get tokens just for making a comment, so award those first
				ftr_name = 'make_comment'
				self.adjustTokens(thing, FEATURES[ftr_name]['value_type'], FEATURES[ftr_name]['ftr_value'], ftr_name, multiplier, buff_or_debuff_type)

				# Now let's process any specifities of that comment	
				if "april fools" in thing.body.lower() or "april foolsday" in thing.body.lower():
					ftr_name = 'say_april_fools'
					self.adjustTokens(thing, FEATURES[ftr_name]['value_type'], FEATURES[ftr_name]['ftr_value'], ftr_name, multiplier, buff_or_debuff_type)
				
				if "you're right" in thing.body.lower() or "youre right" in thing.body.lower() or "you are right" in thing.body.lower():
					ftr_name = 'say_youre_right'
					self.adjustTokens(thing, FEATURES[ftr_name]['value_type'], FEATURES[ftr_name]['ftr_value'], ftr_name, multiplier, buff_or_debuff_type)
				
				if "i agree" in thing.body.lower():
					ftr_name = 'say_i_agree'
					self.adjustTokens(thing, FEATURES[ftr_name]['value_type'], FEATURES[ftr_name]['ftr_value'], ftr_name, multiplier, buff_or_debuff_type)
				
				if "fuck" in thing.body.lower():
					ftr_name = 'say_fuck'
					self.adjustTokens(thing, FEATURES[ftr_name]['value_type'], FEATURES[ftr_name]['ftr_value'], ftr_name, multiplier, buff_or_debuff_type)
				
				if "nigger" in thing.body.lower() or "nigga" in thing.body.lower() or "niggar" in thing.body.lower():
					ftr_name = 'say_nigger'
					self.adjustTokens(thing, FEATURES[ftr_name]['value_type'], FEATURES[ftr_name]['ftr_value'], ftr_name, multiplier, buff_or_debuff_type)
				
				if "token farm" in thing.body.lower() or "farm token" in thing.body.lower():
					ftr_name = 'say_token_farm'
					self.adjustTokens(thing, FEATURES[ftr_name]['value_type'], FEATURES[ftr_name]['ftr_value'], ftr_name, multiplier, buff_or_debuff_type)
				
				if "u/cozmo23 " in thing.body.lower():
					ftr_name = 'say_cozmo23'
					self.adjustTokens(thing, FEATURES[ftr_name]['value_type'], FEATURES[ftr_name]['ftr_value'], ftr_name, multiplier, buff_or_debuff_type)
				
				if "u/cozmo " in thing.body.lower():
					ftr_name = 'say_cozmo'
					self.adjustTokens(thing, FEATURES[ftr_name]['value_type'], FEATURES[ftr_name]['ftr_value'], ftr_name, multiplier, buff_or_debuff_type)
				
				if "u/deej_bng " in thing.body.lower():
					ftr_name = 'say_deej_bng'
					self.adjustTokens(thing, FEATURES[ftr_name]['value_type'], FEATURES[ftr_name]['ftr_value'], ftr_name, multiplier, buff_or_debuff_type)
				
				if "u/deej " in thing.body.lower():
					ftr_name = 'say_deej'
					self.adjustTokens(thing, FEATURES[ftr_name]['value_type'], FEATURES[ftr_name]['ftr_value'], ftr_name, multiplier, buff_or_debuff_type)
				
				if "u/dmg04 " in thing.body.lower():
					ftr_name = 'say_dmg04'
					self.adjustTokens(thing, FEATURES[ftr_name]['value_type'], FEATURES[ftr_name]['ftr_value'], ftr_name, multiplier, buff_or_debuff_type)
				
				if "u/dmg " in thing.body.lower():
					ftr_name = 'say_dmg'
					self.adjustTokens(thing, FEATURES[ftr_name]['value_type'], FEATURES[ftr_name]['ftr_value'], ftr_name, multiplier, buff_or_debuff_type)
				
				if "++" in thing.body.lower():
					ftr_name = 'say_++'
					self.adjustTokens(thing, FEATURES[ftr_name]['value_type'], FEATURES[ftr_name]['ftr_value'], ftr_name, multiplier, buff_or_debuff_type)

				if thing.body.lower() == 'f':
					ftr_name = 'say_f_to_pay_respects'
					self.adjustTokens(thing, FEATURES[ftr_name]['value_type'], FEATURES[ftr_name]['ftr_value'], ftr_name, multiplier, buff_or_debuff_type)


				# Special Features
				if ("happy birthday" in thing.body.lower() and "norse" in thing.body.lower()):
					ftr_name = 'say_happybdaynorse'
					if not self.checkSpecialFeatureUsed(thing.author.name, ftr_name):
						self.adjustTokens(thing, FEATURES[ftr_name]['value_type'], FEATURES[ftr_name]['ftr_value'], ftr_name, multiplier, buff_or_debuff_type)
						self.logSpecialFeatureUsed(thing.author.name, ftr_name)

				if ("gjallarhorn day" in thing.body.lower()):
					ftr_name = 'say_gjallarhorn_day'
					if not self.checkSpecialFeatureUsed(thing.author.name, ftr_name):
						self.adjustTokens(thing, FEATURES[ftr_name]['value_type'], FEATURES[ftr_name]['ftr_value'], ftr_name, multiplier, buff_or_debuff_type)
						self.logSpecialFeatureUsed(thing.author.name, ftr_name)

				if ("gjallarhorn freedom wolfpack pew pew boom day" in thing.body.lower()):
					ftr_name = 'say_gjallarhorn_day_full'
					if not self.checkSpecialFeatureUsed(thing.author.name, ftr_name):
						self.adjustTokens(thing, FEATURES[ftr_name]['value_type'], FEATURES[ftr_name]['ftr_value'], ftr_name, multiplier, buff_or_debuff_type)
						self.logSpecialFeatureUsed(thing.author.name, ftr_name)

				if ("club penguin" in thing.body.lower()):
					ftr_name = 'say_club_penguin'
					if not self.checkSpecialFeatureUsed(thing.author.name, ftr_name):
						self.adjustTokens(thing, FEATURES[ftr_name]['value_type'], FEATURES[ftr_name]['ftr_value'], ftr_name, multiplier, buff_or_debuff_type)
						self.logSpecialFeatureUsed(thing.author.name, ftr_name)

				if ("two tokens and a blue" in thing.body.lower()
					or "2 tokens and a blue" in thing.body.lower()
					or "two tokens & a blue" in thing.body.lower()
					or "2 tokens & a blue" in thing.body.lower()):

					ftr_name = 'say_2taab'
					if not self.checkSpecialFeatureUsed(thing.author.name, ftr_name):
						self.adjustTokens(thing, FEATURES[ftr_name]['value_type'], FEATURES[ftr_name]['ftr_value'], ftr_name, multiplier, buff_or_debuff_type)
						self.logSpecialFeatureUsed(thing.author.name, ftr_name)

			elif isinstance(thing, praw.models.Submission):
				ftr_name = 'make_post'
				self.adjustTokens(thing, FEATURES[ftr_name]['value_type'], FEATURES[ftr_name]['ftr_value'], ftr_name, multiplier, buff_or_debuff_type)

		except Exception as e:
			self.logger.error('{} | Error processing item: {}'.format(self.me, thing.fullname))


	def adjustTokens(self, thing, token_action, token_amount, ftr_name, multiplier, buff_or_debuff_type):
		if multiplier:
			tokens = self.applyBuffOrDebuff(token_amount, multiplier, buff_or_debuff_type)
		else:
			tokens = token_amount

		if token_action == 'add':
			self.logTokens(thing, tokens, ftr_name, 'add', 'Tokens added')
		elif token_action == 'subtract':
			self.logTokens(thing, tokens, ftr_name, 'subtract', 'Tokens removed')
		elif token_action == 'reset':
			current_tokens = self.getUsersTokenAmount(thing.author.name)
			if current_tokens > 0:
				tokens = current_tokens * -1
			else:
				tokens = 0
			self.logTokens(thing, tokens, ftr_name, 'reset', 'Positive tokens removed via trigger')

		self.logger.info('Feature activated: Thing: {} FTR: {} for {}'.format(thing.fullname, ftr_name, thing.author.name))

	def checkNameStartsWith(self, username, character):
		if username.lower().startswith(character):
			return True
		else:
			return False

	def applyBuffOrDebuff(self, token_amount, multiplier, buff_or_debuff_type):
		if buff_or_debuff_type == 'buff_multiplier':
			bonus_tokens = token_amount * multiplier
			new_tokens = token_amount + bonus_tokens
			return new_tokens

		elif buff_or_debuff_type == 'debuff_multiplier':
			bonus_tokens = token_amount * multiplier
			new_tokens = token_amount - bonus_tokens
			return new_tokens

	def logTokens(self, thing, tokens, feature_code, function, note):
		self.logger.debug('logTokens: thing: {}, tokens: {}, feature_code: {}, function: {}, note: {}'.format(thing.fullname, tokens, feature_code, function, note))
		try:
			self.cur.execute('INSERT INTO token_log (timestamp_utc, username, token_amt, note, feature_code, thing_fullname) VALUES (%s, %s, %s, %s, %s, %s)',
				(thing.created_utc, thing.author.name, tokens, note, feature_code, thing.fullname))
		except Exception as e:
			self.logger.error('Error logging to token_log. Thing: {} Feature Code: {}'.format(thing.fullname, feature_code))

	def logSpecialFeatureUsed(self, username, ftr_name):
		if ftr_name == 'say_happybdaynorse':
			field = 'ftr_happybdaynorse'
		elif ftr_name == 'say_club_penguin':
			field = 'ftr_clubpenguin'
		elif ftr_name == 'say_gjallarhorn_day_full':
			field = 'ftr_gjallarhorn_day_full'
		elif ftr_name == 'say_gjallarhorn_day':
			field = 'ftr_gjallarhorn_day'
		elif ftr_name == 'say_2taab':
			field = 'ftr_2taab'

		sql = "INSERT INTO features_log (username, {}) VALUES ('{}', True) ON CONFLICT (username) DO UPDATE SET {} = EXCLUDED.{}".format(field, username, field, field)
		
		try:
			self.cur.execute(sql)
		except Exception as e:
			self.logger.error('Error logging to features_log. SQL: {}'.format(sql))

	def getUsersTokenAmount(self, username):
		self.cur.execute('SELECT username, sum(token_amt) FROM token_log WHERE username=(%s) GROUP BY username', (username, ))
		fetched = self.cur.fetchall()
		if fetched:
			for item in fetched:
				if item[0] == username:
					return item[1]

	def checkSpecialFeatureUsed(self, username, ftr_name):
		self.cur.execute('SELECT username, ftr_happybdaynorse, ftr_clubpenguin, ftr_gjallarhorn_day_full, ftr_gjallarhorn_day, ftr_2taab FROM features_log WHERE username=(%s)', (username, ))
		fetched = self.cur.fetchall()
		if fetched:
			for item in fetched:
				if item[0] == username:
					if ftr_name == 'say_happybdaynorse' and item[1] == True:
						return True
					elif ftr_name == 'say_club_penguin' and item[2] == True:
						return True
					elif ftr_name == 'say_gjallarhorn_day_full' and item[3] == True:
						return True
					elif ftr_name == 'say_gjallarhorn_day' and item[4] == True:
						return True
					elif ftr_name == 'say_2taab' and item[5] == True:
						return True
					else:
						return False

	def update_leaderboard(self):
		table_text = ""
		self.cur.execute('SELECT "username" as "Username", SUM("token_amt") as "Total" FROM "token_log" GROUP BY "username" ORDER BY "Total" DESC LIMIT 50')
		fetched = self.cur.fetchall()
		if fetched:
			for item in fetched:
				tokens_formatted = format(item[1], ',')
				table_text += "/u/{user} | {tokens} \n".format(user=item[0], tokens=tokens_formatted)

			self.update_wiki_page(table_text)

	def update_wiki_page(self, table_text, start_marker="[](#start)", end_marker="[](#end)"):
		content = self.subreddit.wiki[LEADERBOARD_WIKI].content_md

		try:
			start = content.index(start_marker)
			end = content.index(end_marker) + len(end_marker)

			if content[start:end] != "{}{}{}".format(start_marker, table_text, end_marker):
				content = content.replace(content[start:end], "{}{}{}".format(start_marker, table_text, end_marker))
				try:
					self.subreddit.wiki[LEADERBOARD_WIKI].edit(content, reason="Updating leaderboard wiki page")
					self.logger.info("Wiki page {} update for {} SUCCESS".format(LEADERBOARD_WIKI, self.subreddit))
				except:
					self.logger.error("Wiki page {} update for {} FAILED".format(LEADERBOARD_WIKI, self.subreddit))

		except:
			self.logger.exception('Unable to update wiki page: {}/{}'.format(self.subreddit, LEADERBOARD_WIKI))

def main(bot):
	try:
		bot.action()

	except UnicodeEncodeError:
		bot.logger.warning("Caught UnicodeEncodeError")
 
	except praw.exceptions.APIException:
		bot.logger.exception("API Error! - Sleeping")
		time.sleep(120)

	except praw.exceptions.ClientException:
		bot.logger.exception("PRAW Client Error! - Sleeping")
		time.sleep(120)

	except prawcore.exceptions.ServerError:
		bot.logger.exception("PRAW Server Error! - Sleeping")
		time.sleep(120)

	except KeyboardInterrupt:
		bot.logger.warning('Caught KeyboardInterrupt - Exiting')
		bot.canRun = False
		
	except Exception:
		bot.logger.critical('General Exception - sleeping 5 min')
		time.sleep(300)

if __name__ == '__main__':
	bot = TokenBot()
	bot.logger.info("Starting up")
	while bot.canRun:
		main(bot)
