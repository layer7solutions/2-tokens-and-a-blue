# Constants file

FEATURES = {
    'gild_anyone': {
        'value_type': 'add',
        'ftr_value': 25.0
    },
    'is_gilded': {
        'value_type': 'add',
        'ftr_value': 25.0
    },
    'make_post': {
        'value_type': 'add',
        'ftr_value': 7.0
    },
    'make_comment': {
        'value_type': 'add',
        'ftr_value': 3.0
    },
    'say_happybdaynorse': {
        'value_type': 'add',
        'ftr_value': 743.0
    },
    'gild_lobstah': {
        'value_type': 'add',
        'ftr_value': 100.0
    },
    'say_april_fools': {
        'value_type': 'subtract',
        'ftr_value': -10.0
    },
    'say_youre_right': {
        'value_type': 'add',
        'ftr_value': 2.0
    },
    'say_i_agree': {
        'value_type': 'add',
        'ftr_value': 2.0
    },
    'say_fuck': {
        'value_type': 'reset',
        'ftr_value': 0.0
    },
    'say_nigger': {
        'value_type': 'reset',
        'ftr_value': 0.0
    },
    'say_token_farm': {
        'value_type': 'reset',
        'ftr_value': 0.0
    },
    'say_cozmo23': {
        'value_type': 'subtract',
        'ftr_value': -10.0
    },
    'say_cozmo': {
        'value_type': 'add',
        'ftr_value': 2.0
    },
    'say_deej_bng': {
        'value_type': 'subtract',
        'ftr_value': -10.0
    },
    'say_deej': {
        'value_type': 'add',
        'ftr_value': 2.0
    },
    'say_dmg04': {
        'value_type': 'subtract',
        'ftr_value': -10.0
    },
    'say_dmg': {
        'value_type': 'add',
        'ftr_value': 2.0
    },
    'say_++': {
        'value_type': 'subtract',
        'ftr_value': -50.0
    },
    'say_gjallarhorn_day': {
        'value_type': 'add',
        'ftr_value': 50.0
    },
    'say_gjallarhorn_day_full': {
        'value_type': 'add',
        'ftr_value': 500.0
    },
    'say_club_penguin': {
        'value_type': 'add',
        'ftr_value': 49.0
    },
    'username_starts_with_n_buff': {
        'value_type': 'buff_multiplier',
        'ftr_value': 0.33
    },
    'username_starts_with_f_debuff': {
        'value_type': 'debuff_multiplier',
        'ftr_value': 0.4
    },
    'say_2taab': {
        'value_type': 'add',
        'ftr_value': 50.0
    },
    'say_f_to_pay_respects': {
        'value_type': 'subtract',
        'ftr_value': -60.0
    }
}